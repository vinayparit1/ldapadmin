<?php
   include('config.php');
   session_start();
   
   $user_check = $_SESSION['login_user'];
   
   $ses_sql = mysqli_query($db,"select username from admin where username = '$user_check' ");
   
      
   if(!isset($_SESSION['login_user'])){
      header("location:../ldap/Login_v10/login.php");
      die();
   }
?>

<?php
// including the database connection file
include_once("db.php");

if(isset($_POST['update']))
{	

	$ID = mysqli_real_escape_string($mysqli, $_POST['ID']);
	
	$rserver = mysqli_real_escape_string($mysqli, $_POST['rserver']);
	$protocol = mysqli_real_escape_string($mysqli, $_POST['protocol']);
	$rmail = mysqli_real_escape_string($mysqli, $_POST['rmail']);	
	$rpassword = mysqli_real_escape_string($mysqli, $_POST['rpassword']);
	$lmail = mysqli_real_escape_string($mysqli, $_POST['lmail']);
	// checking empty fields
	if(empty($rserver) || empty($protocol) || empty($rmail) || empty($rpassword) || empty($lmail)) {	
			
		if(empty($rserver)) {
			echo "<font color='red'>Remote Server field is empty.</font><br/>";
		}
		
		if(empty($protocol)) {
			echo "<font color='red'>Protocol field is empty.</font><br/>";
		}
		
		if(empty($rmail)) {
			echo "<font color='red'>Remote Email field is empty.</font><br/>";
		}
		if(empty($rpassword)) {
			echo "<font color='red'>Remote Password field is empty.</font><br/>";
		}
		if(empty($lmail)) {
			echo "<font color='red'>Local Email field is empty.</font><br/>";
		}		
	} else {	
		//updating the table
$result = mysqli_query($mysqli, "UPDATE fetchmail SET rserver='$rserver',protocol='$protocol',rmail='$rmail',rpassword='$rpassword',lmail='$lmail' WHERE ID=$ID");
		
		include 'fetchmaildata.php';
		
		//redirectig to the display page. In our case, it is index.php
		header("Location: index.php");
	}
}
?>
<?php
//getting id from url
$ID = $_GET['ID'];

//selecting data associated with this particular id
$result = mysqli_query($mysqli, "SELECT * FROM fetchmail WHERE ID=$ID");

while($res = mysqli_fetch_array($result))
{

	$rserver = $res['rserver'];
	$protocol = $res['protocol'];
	$rmail = $res['rmail'];
	$rpassword = $res['rpassword'];
	$lmail = $res['lmail'];
}
?>
<html>
<head>	
	<meta charset="utf-8">
	<title>Edit data</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
	<script type="text/javascript">
$(document).ready(function () {
    //Disable cut copy paste
    $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
   
    //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });
});
</script>

</head>
<center>
<body>
	<h3><a href="index.php">Go To Home Page!!</a></h3>
	
	
	<form name="form1" method="post" action="edit.php">
		<table border="0">
			<tr> 
				<td>Remote Server</td>
				<td><br/><input class="form-control" type="text" name="rserver" value="<?php echo $rserver;?>"></td>
			</tr>
			<tr> 
				<td>Protocol</td>
				<td><br/><!--input class="form-control" type="text" name="protocol" value="<?php echo $protocol;?>"-->
			<select title="Pick a number" type="text" class="selectpicker form-control" name="protocol" value="<?php echo $protocol;?>">
          	<option value="">Select...</option>
          	<option value="POP3"<?php if( $protocol == 'POP3') { ?> selected="selected"<?php } ?>>POP3</option>
          	<option value="POP3S"<?php if( $protocol == 'POP3S') { ?> selected="selected"<?php } ?>>POP3S</option>
          	<option value="IMAP"<?php if( $protocol == 'IMAP') { ?> selected="selected"<?php } ?>>IMAP</option>
          	<option value="IMAPS"<?php if( $protocol == 'IMAPS') { ?> selected="selected"<?php } ?>>IMAPS</option>
        	</select></td>
				
			</tr>
			<tr> 
				<td>Remote Email</td>
				<td><br/><input class="form-control" type="text" name="rmail" value="<?php echo $rmail;?>"></td>
			</tr>
			<tr> 
				<td>Remote Password</td>
				<td><br/><input class="form-control" type="text" name="rpassword" value="<?php echo $rpassword;?>"></td>
			</tr>
			<tr> 
				<td>Local Email</td>
				<td><br/><input class="form-control" type="text" name="lmail" value="<?php echo $lmail;?>"></td>
			</tr>
			<tr>
				<td><input type="hidden" name="ID" value="<?php echo $_GET['ID'];?>"></td>
				<td><br/><input class="btn btn-success" type="submit" name="update" value="Update"></td>
				
			</tr>
		</table>
	</form>
</body>
</center>
</html>