		<?php
   include('config.php');
   session_start();
   
   $user_check = $_SESSION['login_user'];
   
   $ses_sql = mysqli_query($db,"select username from admin where username = '$user_check' ");
   
   $row = mysqli_fetch_array($ses_sql,MYSQLI_ASSOC);
   
   $login_session = $row['username'];
   
   if(!isset($_SESSION['login_user'])){
      header("location:../ldap/Login_v10/login.php");
      die();
   }
?>
<html>
<head>
	<title>Add Data</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<script src="js/bootstrap.min.js"></script>

	<script type="text/javascript">
$(document).ready(function () {
    //Disable cut copy paste
    $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
   
    //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });
});
</script>

</head>
<center>
<body>
<?php
//including the database connection file
include_once("db.php");

if(isset($_POST['Submit'])) {	
	$rserver = mysqli_real_escape_string($mysqli, $_POST['rserver']);
	$protocol = mysqli_real_escape_string($mysqli, $_POST['protocol']);
	$remail = mysqli_real_escape_string($mysqli, $_POST['rmail']);
	$rpassword = mysqli_real_escape_string($mysqli, $_POST['rpassword']);
	$lemail = mysqli_real_escape_string($mysqli, $_POST['lmail']);
		
	// checking empty fields
	if(empty($rserver) || empty($protocol) || empty($remail) || empty($rpassword) || empty($lemail)) {
				
		if(empty($rserver)) {
			echo "<font color='red'>Remote Server field is empty.</font><br/>";
		}
		
		if(empty($protocol)) {
			echo "<font color='red'>Protocol field is empty.</font><br/>";
		}
		
		if(empty($remail)) {
			echo "<font color='red'>Remote Email field is empty.</font><br/>";
		}
		
		if(empty($rpassword)) {
			echo "<font color='red'>Remote Password field is empty.</font><br/>";
		}

		if(empty($lemail)) {
			echo "<font color='red'>Local Email field is empty.</font><br/>";
		}

		//link to the previous page
		echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
	} else { 
		// if all the fields are filled (not empty) 
			
		//insert data to database	
		$result = mysqli_query($mysqli, "INSERT INTO fetchmail(rserver,protocol,rmail,rpassword,lmail) VALUES('$rserver','$protocol','$remail','$rpassword','$lemail')");
		
		require_once('fetchmaildata.php');
		//include ('fetchmaildata.php');
		//`php fetchmaildata.php`;
		//display success message
		echo "<font color='green'><h3>Data Added Successfully.</h3>";
		echo "<br/><a href='index.php'><h4>View Result</h4></a>";

	}
}
?>
</body>
</center>
</html>
