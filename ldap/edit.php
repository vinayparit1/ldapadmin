<?php
   include('config.php');
   session_start();
   
   $user_check = $_SESSION['login_user'];
   
   $ses_sql = mysqli_query($db,"select username from admin where username = '$user_check' ");
   
      
   if(!isset($_SESSION['login_user'])){
      header("location:Login_v10/login.php");
      die();
   }
?>

<html>
<head>
    <title>Edit data</title>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    
    <script type="text/javascript">
$(document).ready(function () {
    //Disable cut copy paste
    $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
   
    //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });
});
</script>

     <style>
    /* Header Set black background color, white text and some padding */
    header {
      background-color: #1abc9c;
      color: white;
      padding: 10px;
    }

    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
      padding-right: 100px;
      padding-left: 150px;
    }
    .navbar-brand {
      padding-right: 120px;

    }

    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 830px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Footer Set black background color, white text and some padding */
    footer {
      background-color: #212121;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;}
    }

     hr.style5 {
    background-color: #fff;
    border-top: 2px dashed #8c8b8b;
    }
  </style>
</head>
<body>


<!--header class="container-fluid text-center">
  <h3> Welcome To Admin Panel </h3>
</header-->

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="home.html">Home</a></li>
        <li class="#"><a href="useradd.html">Add User</a></li>
        <li><a href="index.php">Browse User</a></li>
        <li><a href="../crud/">Fetch Mail</a></li>
        
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
     <!--  <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p> -->
    </div>
   <div class="col-sm-8 text-left"> 
      <!-- <h1>Welcome</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      <hr>
      <h3>Test</h3>
      <p>Lorem ipsum...</p> -->

<?php

// specify the LDAP server to connect to
$conn = ldap_connect("mail.kplogics.in") or die("Could not connect to server");

// bind to the LDAP server specified above
$r = ldap_bind($conn,"uid=easypush,ou=people,dc=kplogics,dc=in","JdkoaqSN") or die("Could not bind to server");

// set base DN and return attribute list
$base_dn = "ou=people,dc=kplogics,dc=in";
$params = array("mail", "cn", "sn", "uid");

// perform search using email address passed on URL
$result = ldap_list($conn, $base_dn, "mail=" . urldecode($_GET['mail']), $params);

// extract data into array
$info = ldap_get_entries($conn, $result);

// print and display as editable form
  
?>

<!--form method="POST" action="modify.php"-->
  <form method="POST" class="form-horizontal" action="modify.php" >
  <table border="0" cellpadding="0" cellspacing="10" width="500" >
  <br>
    <input type="submit" value="submit" class="btn btn-success btn-md">     
      <hr class="style5">
      <div class="form-group">
      <label class="control-label col-sm-2" for="loginname">First Name:</label>
      <div class="col-xs-4">
        <input style="text-transform: lowercase" type="text" class="form-control" id="cn" placeholder="Login Name" name="cn" value="<?php echo $info[0]["cn"][0]; ?>">
      </div>
      </div>

      <div class="form-group">
      <label class="control-label col-sm-2" for="firstname">Last Name:</label>
      <div class="col-xs-4">
        <input style="text-transform: lowercase" type="text" class="form-control" id="sn" placeholder="First Name" name="sn" value="<?php echo $info[0]["sn"][0]; ?>">
      </div>
      </div>

      <div class="form-group">
      <label class="control-label col-sm-2" for="firstname">E-mail:</label>
      <div class="col-xs-4">
        <input style="text-transform: lowercase" type="text" class="form-control" id="mail" placeholder="First Name" name="mail" value="<?php echo $info[0]["mail"][0]; ?>">
      </div>
      </div>

      <div class="form-group">
      <label class="control-label col-sm-2" for="firstname">UID:</label>
      <div class="col-xs-4">
        <input style="text-transform: lowercase" type="text" class="form-control" id="uid" placeholder="First Name" name="uid" value="<?php echo $info[0]["uid"][0]; ?>">
      </div>
      </div>

          <td width="100%" colspan="2" align="center">
        <!--input type="submit" value="Submit" name="Submit">
      &nbsp;&nbsp;<input type="reset" value="Reset" name="Reset"-->
      </td>
    </tr>
  </table>
</form>

<?php
// all done? clean up
ldap_close($conn);
?>

</div>
    <div class="col-sm-2 sidenav">
      <!-- <div class="well">
        <p>ADS</p>
      </div>
      <div class="well">
        <p>ADS</p>
      </div> -->
    </div>
  </div>
</div>

<footer class="container-fluid text-center">
  <p>Footer Text</p>
</footer>

  



</body>
</html>