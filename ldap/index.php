<?php
   include('config.php');
   session_start();
   
   $user_check = $_SESSION['login_user'];
   
   $ses_sql = mysqli_query($db,"select username from user where username = '$user_check' ");
   
   //$row = mysqli_fetch_array($ses_sql,MYSQLI_ASSOC);
   
   //$login_session = $row['username'];
   
   if(!isset($_SESSION['login_user'])){
      header("location:Login_v10/login.php");
      die();
   }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="jquery.twbsPagination.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.3.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

<title>Browse users</title>
<!--link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous"-->
<link href="jquery.paginate.css" rel="stylesheet" type="text/css">
<script type="jquery.tabledit.min.js"></script>

<script src="//code.jquery.com/jquery.min.js"></script>
<script src="src/table2csv.js"></script>

<script type="text/javascript">
  
  $(document).ready(function () {

    function exportTableToCSV($table, filename) {

        var $rows = $table.find('tr:has(td)'),

            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            tmpColDelim = String.fromCharCode(11), // vertical tab character
            tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
            colDelim = '","',
            rowDelim = '"\r\n"',

            // Grab text from table into CSV formatted string
            csv = '"' + $rows.map(function (i, row) {
                var $row = $(row),
                    $cols = $row.find('td');

                return $cols.map(function (j, col) {
                    var $col = $(col),
                        text = $col.text();

                    return text.replace(/"/g, '""'); // escape double quotes

                }).get().join(tmpColDelim);

            }).get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim) + '"';

        // Deliberate 'false', see comment below
        if (false && window.navigator.msSaveBlob) {

            var blob = new Blob([decodeURIComponent(csv)], {
                type: 'text/csv;charset=utf8'
            });
            
            // Crashes in IE 10, IE 11 and Microsoft Edge
            // See MS Edge Issue #10396033: https://goo.gl/AEiSjJ
            // Hence, the deliberate 'false'
            // This is here just for completeness
            // Remove the 'false' at your own risk
            window.navigator.msSaveBlob(blob, filename);
            
        } else if (window.Blob && window.URL) {
            // HTML5 Blob        
            var blob = new Blob([csv], { type: 'text/csv;charset=utf8' });
            var csvUrl = URL.createObjectURL(blob);

            $(this)
                .attr({
                    'download': filename,
                    'href': csvUrl
                });
        } else {
            // Data URI
            var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

            $(this)
                .attr({
                    'download': filename,
                    'href': csvData,
                    'target': '_blank'
                });
        }
    }

    // This must be a hyperlink
    $(".export").on('click', function (event) {
        // CSV
        var args = [$('#myTable>table'), 'export.csv'];
        
        exportTableToCSV.apply(this, args);
        
        // If CSV, don't do event.preventDefault() or return false
        // We actually need this to be a typical hyperlink
    });
});
</script>


<script>

      /* search */
$(document).ready(function(){
  $("#username").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});

</script>

<script type="text/javascript">
$(document).ready(function () {
    //Disable cut copy paste
    $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
   
    //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });
});
</script>

     <style>
    /* Header Set black background color, white text and some padding */
    header {
      background-color: #1abc9c;
      color: white;
      padding: 0px;

    }

    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
      padding-right: 100px;
      padding-left: 400px;

    }
    .navbar-brand {
      padding-right: 120px;
    }

    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 830px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #ffffff;
      height: 100%;
    }
    
    /* Footer Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;}
    }

    hr.style5 {
    background-color: #fff;
    border-top: 2px dashed #8c8b8b;
    }

  </style>
</head>
<body>


<!--header class="container-fluid text-center">
  <h3> Welcome To Admin Panel </h3>
</header-->

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
      	<li><a href="home.html">Home</a></li>
        <li><a href="useradd.html">Add User</a></li>
        <li class="active"><a href="index.php">Browse User</a></li>
        <li><a href="../crud/">Fetch Mail</a></li>

      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="../logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
                
      </ul>

    </div>
  </div>
</nav>
  
  &nbsp; 
 &nbsp;

<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
     <!--  <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p> -->
    </div>
   <div class="col-sm-8 text-left"> 

    <div class="col-sm-3">
                <div class="form-group">
                  <br>
                  <label><b>Search User</b></label>
                </br>
                  <div class="form-group has-feedback has-search">
                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                    <input type="text" name="username" id="username" class="form-control" placeholder="Enter user name">

                    </div>
                    
                                   
                </div>
              </div>

      <!-- <h1>Welcome</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      <hr>
      <h3>Test</h3>
      <p>Lorem ipsum...</p> -->

  <br>
  <br>
<table class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">

<?php

// specify the LDAP server to connect to
$conn = ldap_connect("mail.kplogics.in") or die("Could not connect to server");

// bind to the LDAP server specified above
$r = ldap_bind($conn,"uid=easypush,ou=people,dc=kplogics,dc=in","JdkoaqSN") or die("Could not bind to server");

// set base DN and required attribute list
$base_dn = "ou=people,dc=kplogics,dc=in";
$params = array("mail", "cn", "sn" , "uid");

// list all entries from the base DN
$result = ldap_list($conn, $base_dn, "mail=*", $params);

?>

<thead>

    <tr>
        <th><b>Login ID</b></th>
        <th><b>First Name</b></th>
        <th><b>Last Name</b></th>
         <th><b>Mail ID</b></th>
         <th colspan=2><b>&nbsp; Action</b></th>

    </tr>
    </thead>

    <tbody id="myTable">
      
<?php

// get entries
$info = ldap_get_entries($conn, $result);

// and print attribute values
for ($i=0; $i<$info["count"]; $i++) {
    echo "<tr>";
    echo "<td>".$info[$i]["uid"][0]."</td>";
    echo "<td>".$info[$i]["cn"][0]."</td>";
    echo "<td>".$info[$i]["sn"][0]."</td>";
    echo "<td>".$info[$i]["mail"][0]."</td>";
    echo "<td><a href=\"edit.php?mail=" . urlencode($info[$i]["mail"][0]) . "\" class='btn btn-success btn-sm'>Edit</a></td>";
    echo "<td><a href=\"delete.php?uid=" . urlencode($info[$i]["uid"][0]) . "\" onClick=\"return confirm('Are you sure you want to delete?')\" class='btn btn-danger btn-sm'>Delete</a></td>";
    echo "</tr>";
}

// all done? clean up
ldap_close($conn);

?>
</tbody>

<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="jquery.paginate.js"></script>
<script>
$(document).ready(function () {
    $('table').paginate({
        'elemsPerPage': 14,
            'maxButtons': 6
    });
});
</script>
</div><script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</table>

    
    </div>
    <div class="col-sm-2 sidenav">
      <!-- <div class="well">
        <p>ADS</p>
      </div>
      <div class="well">
        <p>ADS</p>
      </div> -->
    </div>
  </div>
</div>

<footer class="container-fluid text-center">
  <p>Footer Text</p>
</footer>
</body>
</html>
